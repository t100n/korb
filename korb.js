/*
 * Initialize KORB
 */
const 	Korb = require('./lib/korb.js'),
		className = 'korb'
		;

console.log(new Date(), className, 'initializing');

Korb.init({}, function(err) {
	
	if(err) {

		console.log(new Date(), className, 'failed to start', err);
		process.exit(0);

	}//if

	console.log(new Date(), className, 'is up and running');

});
