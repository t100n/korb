/*
 * KORB - Keep On Rolling Baby
 * 
 * Make sure your processes keep on running even if they crash for no reason.
 * 
 */
const 	_ = require('lodash'),
		className = 'korb',
        debug = require('debug')('korb'),
        cp = require('child_process'),
        spawn = require('child_process').spawn
		;

var defaultOptions = {
	processesPath: '../processes',
	processFindCmd: "ps aux | grep '{processname}' | awk '{print $2}'",
	processes: {}
};

function Korb() {

	/*
	 * Initialize the options object and load watchers for the processes in ./processes
	 * 
	 * @param options Object
	 * @param cb Function
	 */
	this.init = function(options, cb) {

		try {

			this.options = _.defaults(options || {}, defaultOptions);
			this._loadProcesses(cb);

			debug(new Date(), process.pid, 'init', this.options);

		} catch(err) {
			
			if(typeof cb == "function") {
				return cb(err);
			}//if

		}

	};

	/*
	 * Create watcher processes to watch over each process
	 * 
	 * @param cb Function
	 */
	this._loadProcesses = function(cb) {

		try {

			// Kill already running process watchers
			if(this.options.processes) {
				
				debug(new Date(), process.pid, '_loadProcesses', 'kill already running processes');

				// Kill those processes
				for(var key in this.options.processes.length) {
					process.kill(-this.options.processes[key].pid);
				}//for

			}//if

			this.options.processes = {};

			// Load process configs
			var normalizedPath = require("path").join(__dirname, this.options.processesPath);
			require("fs").readdirSync(normalizedPath).forEach((file) => {
				
				this._createProcess(file);

			});

			if(typeof cb == "function") {
				return cb(null);
			}//if

		} catch(err) {

			if(typeof cb == "function") {
				return cb(err);
			}//if

		}

	};

	/*
	 * Create a watcher for the process
	 * 
	 * @param file String
	 * @param cb Function
	 */
	this._createProcess = function(file, cb) {

		var processConfig = require(require("path").join(this.options.processesPath, file));

		/*
		// Spawn a watcher for the current process config
		this.options.processes[processConfig.name] = spawn('node', [
			require("path").join(__dirname, 'watcher.js'),
			this.options.processFindCmd.replace('{processname}', processConfig.processName),
			processConfig.intervalMS,
			processConfig.timeout,
			processConfig.retries,
			processConfig.recoverySteps
		], {maxBuffer: 1024 * 1024, detached: false});
		this.options.processes[processConfig.name].stdout.on('data', (data) => {
			//debug(new Date(), process.pid, `stdout: ${data}`);
			console.log(new Date(), className, processConfig.name, `${data}`);
		});
		this.options.processes[processConfig.name].stderr.on('data', (data) => {
			//debug(new Date(), process.pid, `stderr: ${data}`);
			console.error(new Date(), className, processConfig.name, `${data}`);
		});
		this.options.processes[processConfig.name].on('close', (code, signal) => {
			//debug(new Date(), process.pid, `child process exited with code ${code} and signal ${signal}`);
			console.log(new Date(), className, processConfig.name, `watcher process exited with code ${code} and signal ${signal}`);
		});
		*/

		this.options.processes[processConfig.name] = cp.fork(require("path").join(__dirname, 'watcher.js'));
		this.options.processes[processConfig.name].send({
			name: processConfig.name,
			distributed: processConfig.distributed,
			channel: processConfig.channel,
			redisHost: processConfig.redisHost,
			redisPort: processConfig.redisPort,
			command: this.options.processFindCmd.replace('{processname}', processConfig.processName),
			intervalMS: processConfig.intervalMS,
			timeout: processConfig.timeout,
			retries: processConfig.retries,
			recoverySteps: processConfig.recoverySteps
		});

		if(typeof cb == "function") {
			return cb();
		}//if

	};

};

module.exports = new Korb();