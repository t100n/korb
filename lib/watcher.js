/*
 * The Watcher thats watchs over a process and tries to recover it if it is not running
 * Distributed recovery - we may have to recover from crashes on other servers
 * 
 */
const 	_ = require('lodash'),
		fs = require('fs'),
		className = 'watcher',
		debug = require('debug')('korb'),
		spawn = require('child_process').spawn,
		exec = require('child_process').exec,
		Redis = require('ioredis'),
		command = process.argv[2],
		intervalMS = process.argv[3],
		timeout = process.argv[4],
		retries = process.argv[5],
		recoverySteps = process.argv[6]
		;

if (!String.prototype.trim) {
  (function() {
    // Make sure we trim BOM and NBSP
    var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
    String.prototype.trim = function() {
      return this.replace(rtrim, '');
    };
  })();
}

var defaultOptions = {
	name: className,
	distributed: 'publisher',
	channel: 'dummy',
	redisHost: 'localhost',
	redisPort: 6379,
	command: '',
	interval: false,
	failed: 0,
	intervalMS: 60000,
	timeout: 60000,
	retries: 0,
	recoverySteps: []
};

function Watcher(options) {
	this.options = _.defaults(options || {}, defaultOptions);
	debug(new Date(), this.options.name, process.pid, this.options);

	/*
	 * Runs the command to check if the process is alive.
	 * If it failed enough times try to recover the process.
	 *
	 * TODO: Check if it is a zombie, kill it and recover.
	 * 
	 */
	this._run = function() {

		exec(this.options.command, (err, stdout, stderr) => {
			
			var pid = parseInt(stdout);
			debug(new Date(), this.options.name, process.pid, 'run', err, isNaN(pid) ? false : pid, stderr);

			// Increment / Reset failed counter
			if(!pid || isNaN(pid)) {
				this.options.failed++;
			}//if
			else {
				this.options.failed = 0;
			}//else

			debug(new Date(), this.options.name, process.pid, 'run', this.options.failed > this.options.retries);

			// Execute the recovery steps
			if(this.options.failed > this.options.retries) {

				this._recover();
				this._notify();

			}//if

		});

	};

	/*
	 * Preform the recovery steps
	 */
	this._recover = function() {

		console.warn(new Date(), this.options.name, process.pid, 'is down');

		clearInterval(this.options.interval);

		for(var i=0, n=this.options.recoverySteps.length; i<n; i++) {

			debug(new Date(), this.options.name, process.pid, 'recovery step', this.options.recoverySteps[i]);

			exec(this.options.recoverySteps[i], (err, stdout, stderr) => {

				//debug(new Date(), this.options.name, process.pid, 'recovery step callback', err, stdout, stderr);

				if(err) console.error(new Date(), this.options.name, process.pid, 'failed to recover', stderr);
				else console.log(new Date(), this.options.name, process.pid, 'recovered');

			});

		}//for

		// Give some time for everything to recover
		setTimeout(() => {
			this.watch();
		}, this.options.timeout);

	};

	/*
	 * Notify the channel about the down state
	 */
	this._notify = function() {

		if(this.options.distributed == 'publisher') {

			this.options.redisClientPub.publish(className+'_'+this.options.channel, this.options.name+' is down');

		}//if

	};

	/*
	 * Initialize the redis client
	 */
	this._createRedisClient = function() {

		console.log(new Date(), this.options.name, process.pid, 'create clients');

		if(!this.options.redisClientPub) {

			this.options.redisClientPub = new Redis(this.options.redisPort, this.options.redisHost, {
				dropBufferSupport: true,
				port: this.options.redisPort,
				host: this.options.redisHost,
				// This is the default value of `retryStrategy`
				retryStrategy: function (times) {
					var delay = times * 2 * 100;
					return delay;
				},
				reconnectOnError: function (err) {
					console.log.error(new Date(), this.options.name, process.pid, 'redis reconnectOnError error', err);

					return true; // or `return 1;`
				}
			});

		}//if

		if(!this.options.redisClientSub) {

			this.options.redisClientSub = new Redis(this.options.redisPort, this.options.redisHost, {
				dropBufferSupport: true,
				port: this.options.redisPort,
				host: this.options.redisHost,
				// This is the default value of `retryStrategy`
				retryStrategy: function (times) {
					var delay = times * 2 * 100;
					return delay;
				},
				reconnectOnError: function (err) {
					console.log.error(new Date(), this.options.name, process.pid, 'redis reconnectOnError error', err);

					return true; // or `return 1;`
				}
			});

		}//if
		
	};

	/**
	 * Subscribe to the process channel
	 */
	this._subscribeChannel = function(cb) {

		console.log(new Date(), this.options.name, process.pid, 'subscribe channel', className+'_'+this.options.channel);

		this.options.redisClientSub.subscribe(className+'_'+this.options.channel);
		this.options.redisClientSub.on('message', (channel, message) => {

			return cb(channel, message);

		});

	};

	/*
	 * Create the checker that runs at a given interval
	 * 
	 */
	this.watch = function() {

		// Stop any existing checker
		clearInterval(this.options.interval);

		if(this.options.distributed) {

			// Create a redis client
			this._createRedisClient();

			if(this.options.distributed == 'subscriber') {

				// Subscribe to channel
				this._subscribeChannel((channel, message) => {

					console.log(new Date(), this.options.name, process.pid, 'message');

					// Something wrong happened. Lets Recover!
					this._recover();

				});

			}//if

		}//if

		// Create the checker in the given interval
		this.options.interval = setInterval(() => {

			this._run();

		}, this.options.intervalMS);

		this._run();

	};
};

process.on('message', (m) => {

	var watcher = new Watcher(m);
	watcher.watch();

});

/*
var watcher = new Watcher({
	command: command,
	intervalMS: intervalMS,
	timeout: timeout,
	retries: retries,
	recoverySteps: recoverySteps ? recoverySteps.split(',') : []
});

watcher.watch();
*/
