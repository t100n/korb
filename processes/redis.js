function RedisProcess() {
	this.name = "redis";
	this.distributed = 'publisher';
	this.channel = "redis-server";
	this.redisHost = "localhost",
	this.redisPort = 6379,
	this.processName = "[r]edis-server"; // Hack for not listing the grep process in ps
	this.intervalMS = 60000;
	this.retries = 0;
	this.timeout = 60000;
	this.recoverySteps = [
		'service redis restart',
		'service supervisord restart'
	];
};

module.exports = new RedisProcess();
