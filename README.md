# KORB #

KORB - Keep On Rollin Baby

Make sure your processes keep on running even if they crash for no reason.

### What is it for? ###

You want to watch over a process in your system and preform a set of actions to recover it, if the process goes down.

Do you have a server that must handle the crash of a process in another server? No problem we can deal with that too!

In KORB you can create configuration files defining the process and those set of actions and a watcher will take care of KORBing it.

### How do I get set up? ###

Clone this repo.

`cd korb`

`npm install`

### How do I run it? ###

You can simply run it like `node korb.js`.

If you want to see the debug messages you can run it like `DEBUG=korb node korb.js`.

You have to make sure the user you are running korb.js has permissions to execute the recoverySteps commands!

### How do I create Watchers? ###

All watchers reside in the `processes` folder. You can use the `redis.js` file as a base for your custom files.

The file strucure is the following:

* name - String
> The name of the watcher. Must be unique.
* processName - String
> The name of the process you want to watch over.
* intervalMS - Number
> The interval in milliseconds to check if the process is still alive.
* retries - Number
> The number of retries until executing the recovery steps.
* timeout - Number
> The time in milliseconds to give the recovery steps until resuming checking if the process is still alive.
* recoverySteps - Array
> The list of recovery steps commands to execute when a process is dead.
* distributed - String[publisher|subscriber]
> Need a distributed recovery? Set the watcher as a publisher (notifies other servers they need to recover) or a subscriber (handle crashes of another server).
* channel - String
> The channel to wish we must publish the down state so other servers may recover, and the channel other servers must listen.
* redisHost - String
> The redis host address
* redisPort - String
> The redis port
